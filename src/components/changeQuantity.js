import React from 'react';
import { Button, Form } from 'react-bootstrap';

export default function ChangeQuantity({ order, setOrder }) {
  const increment = () => setOrder({ ...order, quantity: order.quantity + 1 });
  const decrement = () => setOrder({ ...order, quantity: order.quantity - 1 });

  return (
    <div className="container my-4">
      <div className="input-group quantity d-inline-flex align-items-center position-relative">
        <Button
          variant="outline-danger"
          className="btn-rounded-left"
          onClick={decrement}
          disabled={order.quantity <= 1}
        >
          -
        </Button>
        <Form.Control
          type="number"
          name="quantity"
          value={order.quantity}
          onChange={(e) => {}}
          className="text-center"
        />
        <Button
          variant="outline-success"
          className="btn-rounded-right"
          onClick={increment}
        >
          +
        </Button>
      </div>
    </div>
  );
}