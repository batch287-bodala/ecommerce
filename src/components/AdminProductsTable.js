import { Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AdminProductsTable({ product }) {
  const { name, description, price, _id, isActive } = product;
  const navigate = useNavigate();

  const toggleProductStatus = (e, productId, activate) => {
    const actionText = activate ? 'Activate' : 'Deactivate';

    Swal.fire({
      title: `Are you sure you want to ${actionText.toLowerCase()} this product?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: 'primary',
      cancelButtonColor: 'red',
      confirmButtonText: `Yes, ${actionText.toLowerCase()} it`
    }).then(result => {
      if (result.isConfirmed) {
        const endpoint = activate
          ? `${process.env.REACT_APP_API_URL}/products/${productId}/activate`
          : `${process.env.REACT_APP_API_URL}/products/${productId}/archive`;

        fetch(endpoint, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
            if (data) {
              Swal.fire({
                title: `${actionText}d!`,
                text: `The product has been ${actionText.toLowerCase()}d.`,
                icon: 'success'
              });
              window.location.reload(true);
              navigate('/admin');
            } else {
              Swal.fire({
                title: 'Something went wrong!',
                icon: 'error',
                text: 'Please try again'
              });
            }
          });
      }
    });
  };

  return (
    <tr>
      <td>{name}</td>
      <td>{description}</td>
      <td>₹{price}</td>
      <td>
        <Button className="m-1" variant="primary" as={Link} to={`/products/${_id}/update`}>
          Update
        </Button>
        {isActive ? (
          <Button variant="danger" onClick={e => toggleProductStatus(e, _id, false)}>
            Deactivate Product
          </Button>
        ) : (
          <Button variant="success" onClick={e => toggleProductStatus(e, _id, true)}>
            Activate Product
          </Button>
        )}
      </td>
    </tr>
  );
}
