import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import AdminProducts from './AdminProducts';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';

export default function Admin() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  if (user.isAdmin === true) {
    return (
      <>
        <Container className="d-flex justify-content-center">
          <div>
            <h2 className="text-center">Admin Dashboard</h2>
            <Container className="d-flex justify-content-center">
              <Col xs={6}>
                <Button variant="primary" as={Link} to="/addProduct">
                  Add New Product
                </Button>
              </Col>
              <Col xs={8}>
                <Button variant="success">Show User Orders</Button>
              </Col>
            </Container>
          </div>
        </Container>
        <Row className="mt-3 mb-3">
          <AdminProducts />
        </Row>
      </>
    );
  } else {
    return null;
  }
}
