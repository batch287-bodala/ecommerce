import { Table } from 'react-bootstrap';
import AdminProductTableRow from '../components/AdminProductsTable';
import { useState, useEffect } from 'react';

export default function AdminProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all-products`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      });
  }, []);

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {products.map(product => (
          <AdminProductTableRow key={product._id} product={product} />
        ))}
      </tbody>
    </Table>
  );
}
