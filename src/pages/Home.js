import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
		title: "E-Book Store",
		content: "Discover the World of Words",
		destination: "/products",
		label: "Order Now!"
	};

	return(
		<>
			<Banner data={data} />
			<Highlights />
		</>
	)
};